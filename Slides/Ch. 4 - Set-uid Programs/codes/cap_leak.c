#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>

int main()
{
	int fd = -1;	

	fd = open("/etc/zzz", O_RDWR | O_APPEND);
	if(fd == -1)
	{
		printf("Cannot open file");
		return 0;
	}

	printf("fd is %d\n", fd);

	setuid(getuid());

	char * v[2];
	v[0] = "/bin/sh";
	v[1] = 0;
	execve(v[0], v, 0);
}
