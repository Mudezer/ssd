#include <iostream>
#include <stdlib.h>

using namespace std;

int main()
{
    int i = 0;
    int canari1 = 0;
    int canari2 = 0;

    while(i != -1)
    {        
        if(i != canari1 + canari2)
        {
            cout << "CHEATER" << endl;
            break;
        }

        cout << "Type -1 to exit, -2 to print i, sth else to change value" << endl;        
        int j;
        cin >> j;
        if(j == -2)
            cout << "i = " << i << endl;
        else
        {
            i = j;
            canari1 = rand() % 100 + 1;
            canari2 = i - canari1;            
        }
    }
}
