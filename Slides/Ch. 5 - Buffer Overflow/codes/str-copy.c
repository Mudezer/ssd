#include "stdio.h"
#include "string.h"

void doStuff(const char* s)
{
	char dest[13];
	strcpy(dest, s); //ok
		
	char dest2[12];
	strcpy(dest2, s);  //overflow
		
	const char* s2 = "My really long beautiful string";
	strcpy(dest2, s2); //overflow
}

int main()
{
	const char* s  = "Hello there!";
	
	printf(%zu, strlen(s)); //12
	printf(%zu, sizeof s);  //13 -> null terminator included		
	
	doStuff(s);
}