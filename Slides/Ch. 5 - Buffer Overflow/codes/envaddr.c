#include <stdio.h>
#include <unistd.h>

int main()
{
	char * shell = (char*) getenv("MYSHELL");
	
	if(shell)
	{
		printf("Value   : %s\n", shell);
		printf("Address : %p\n", shell);
	}
}
