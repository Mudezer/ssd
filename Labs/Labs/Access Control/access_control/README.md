# SECG4 - Access Control

## Build and run
Build the jar files:
```shell
mvn package
```

It should have build `target/server.jar` and `target/client.jar`. You can then
run it
```shell
java -jar target/server.jar
```
And
```shell
java -jar target/client.jar
```
