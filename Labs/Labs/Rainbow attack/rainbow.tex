\documentclass[a4paper,11pt]{article}

%\usepackage[showframe]{geometry} %use this if you want to check margins
\usepackage{geometry}
\geometry{centering,total={160mm,250mm},includeheadfoot}

%auxiliairy files
\input{header.tex} %usepackage and configurations
\input{cmds.tex} %user defined commands

%title
\newcommand{\doctitle}{SSD \& WS}
\newcommand{\docsubtitle}{Homework : rainbow attack}
\newcommand{\tdyear}{2022 - 2023}
\author{R. Absil}

%language input
\lstset{language=c++,
		morekeywords={constexpr,nullptr}}

\newcommand{\savefootnote}[2]{\footnote{\label{#1}#2}}
\newcommand{\repeatfootnote}[1]{\textsuperscript{\ref{#1}}}

\newcommand{\deadlinesubscription}{February 26 at 23h59}
\newcommand{\deadline}{March 5 at 23h59}

%language={[x86masm]Assembler}
%language=c++, morekeywords={constexpr,nullptr}
%language=Java

\begin{document}
\pagestyle{fancy} %displays custom headers and footers

\maketitle

The objective of this group (3-5 students) homework is to implement an attack on password tables with a rainbow table. The deadline is set on \deadline.

\section*{Minimal objectives}

From a table of passwords stored as pairs ``(login,hash)'' with the help of some cryptographic function $H$, you must implement a rainbow attack.

For academic reasons (mainly simplicity),
\begin{itemize}
\item passwords are \emph{not} salted,
\item passwords are stored after a single pass through the hash function,
\item passwords are alphanumeric with length\footnote{You are allowed to build a rainbow table per password length, for simplicity reasons.} at least 6 and at most 10,
\item the hash function $H$ is SHA-256.\\
\end{itemize}

The choice of language is left to your discretion (but that choice is your responsibility). Should you use custom libraries, their code \emph{must} be open-source.

Please note that you must at least submit two scripts and one text file :
\begin{itemize}
\item a preprocessing script allowing to generate a ``sufficiently large''\footnote{The user can decide what is ``sufficiently large''.} rainbow table $RT$,
\item an attack script allowing to exploit $RT$ in order to find passwords from their hashes.% in the password table $T$,
%\item a \texttt{README} file explaining the set of commands necessary to compile and run your scripts, as well as installing missing third party libraries.
\end{itemize}
In \emph{no way} you are allowed to submit the rainbow table $RT$, which can be quite large.

For the sake of uniformity, your attack script must allow to input hashes stored in a text file, one hash per line, written as a base-16 string of length 64.

Should you find it useful, two scripts are provided for you:
\begin{itemize}
\item \texttt{gen-passwd}, generating passwords accepted by the policy, storing them in one text file, and their hashes in another file,
\item \texttt{check-passwd}, checking whether passwords stored in one file match hashes stored in another file.
\end{itemize}
You can compile them using the commands
\begin{lstlisting}
g++ -o gen-passwd -std=XXX random.hpp sha256.cpp gen-passwd.cpp passwd-utils.hpp
g++ -o check-passwd -std=XXX random.hpp sha256.cpp check-passwd.cpp passwd-utils.hpp
\end{lstlisting}
where \texttt{XXX} is assumed to refer at least \texttt{c++17}. Running these programs without command line arguments will provide further information about how to use them. You will also find an implementation of a thread pool should you find it useful\footnote{It is unlikely that you will meet the efficiency requirements detailed later without multithreading.}.%\footnote{I suspect my implementation is vulnerable to spurious wakeups. That shouldn't be a problem.}

You will also find an open source \texttt{C++} implementation of SHA-256. A \texttt{main} file also shows how to use this implementation.

\section*{Submission modalities}

%In order \emph{not} to get 0/20, the following requirements have to be met:
%\begin{itemize}
%\item you need to host your project in a gitlab\footnote{That is, not a github one} repository where you will have added me (\texttt{rabsil}) with administrator rights,
%\item you need send me an email\footnote{The automatic notifications provided by gitlab are not enough.} detailing the composition of your group and giving me the SSH URL\footnote{A git SSH URL looks like \texttt{git@gitlab.com:username/projectname.git}.} to your gitlab repository at most ,
%\item you need to provide a README file in your repository explaining how to build your project (which includes installing missing dependencies as well as compiling your project),
%\item the commands provided in your README file must allow your project to be built without errors.
%\end{itemize}
%
%All submitted repositories will be pulled on \deadline\ + 5 minutes. No delayed submissions of any kind will be graded.
Projects have to be implemented in groups of 3 to 5 students, and submitted with the help of a gitlab\footnote{That is, not a github repository.} repository\footnote{Create the repository yourself, add me (\texttt{rabsil}) as maintainer.}. For that purpose, send me an email on \deadlinesubscription\ at the latest with the ssh URL\footnote{A gitlab ssh URL looks like \texttt{git@gitlab.com:username/projectname.git}~.} to your repository\footnote{The automatic email notification is \emph{not} enough.}, and the name and matricule of your group members.

You have to submit your work on \deadline\ at the latest. The minimal requirements for submitted projects are as follows:
\begin{itemize}
  \item projects have to be submitted on time,
  \item projects have to provide a \texttt{README} file
    \begin{itemize}
      \item mentioning the name and matricule of your group members,
      \item explaining how to build\footnote{It is expected that your script installs missing dependencies in addition to compiling your code.} your project on a ubuntu 22.04 distribution (we recommend here to either provide a makefile, or a shell script to install missing dependencies, compile the project and run relevant scripts),
      \item explaining how to use your project (for example, ``to launch the attack, type the following command in a shell'').
    \end{itemize}
\end{itemize}

Projects failing to meet these requirements will not be graded (that is, they will get 0/20). In particular, projects that do not compile according to your \emph{exact} instructions will not be graded. Furthermore, note that we shall in \emph{no way} build or run your projects in an IDE.

Note that these above conditions are necessary but clearly not sufficient to get 10/20. To increase your chances of successfully complete this homework, I would strongly advise
\begin{itemize}
\item to be able to generate a sufficiently large rainbow table under one night of user time on a laptop\savefootnote{fn:no-power}{That is, you cannot reasonably assume I have a computing cluster at my disposal, nor that my machine will behave fairly if you load computations on the GPU.},
\item not to generate a rainbow table bigger than 12 GB,
\item to be able to successfully crack $50$\% of a set of hashes ($\simeq$ 100) provided as a text file\footnote{Recall that passwords are alphanumeric (lower and upper case) with length at least 6 and at most 10, are stored unsalted after a single pass to the SHA-256 hash function.} under 45min of CPU time on a laptop\repeatfootnote{fn:no-power}.\\
\end{itemize}

It is forbidden to cry and forbidden to laugh.

%This project is a \emph{group project}, a single submission per group of 3-5 students is enough. The deadline is set on \deadline, and you have to send

\end{document}

